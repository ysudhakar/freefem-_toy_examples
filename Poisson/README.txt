//====================================================
//  Author: Y Sudhakar
//  email : iitm.sudhakar@gmail.com
//====================================================

Solve a Poison equation using Freefem++.

See description.pdf for the Problem definition and the associated weakforms

The folders contain the following codes:
1. Strong_BC --- Strong enforcement of Dirichlet BC
2. LagrangeMultiplier_BC --- Dirichlet condition using Lagrange multipliers
3. Nitsche_BC --- Dirichlet condition using Nitsche's method (both symmetric and nonsymmetric)
