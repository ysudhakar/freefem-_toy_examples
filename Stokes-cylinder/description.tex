\documentclass{article}
\usepackage[hmargin=2.5cm,vmargin=3cm]{geometry}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{bm}
\usepackage{setspace}

\renewcommand{\baselinestretch}{1.5}

\renewcommand{\familydefault}{\sfdefault}

\begin{document}
\title{Finite element discretization of Stokes equations}
\author{Y. Sudhakar}
\maketitle

\section{Stress formulation}
The governing equations together with the boundary conditions are
\begin{align*}
-\bm{\nabla}\cdot \bm{\sigma}&=\bm{b}\\
\bm{\nabla}\cdot \bm{u}&=0\\
\bm{u}&=\bm{u_D} \ \ \ on\ \Gamma_D\\
\bm{\sigma}\cdot \bm{n}&=\bm{t} \ \ \ on\ \Gamma_N\\
\end{align*}
where, the stresses are given as,
\begin{align*}
\bm{\sigma}&=-p\textbf{I}+\bm{\tau}\\
                   &=-p\textbf{I}+\mu\left(\bm{\nabla}\bm{u}+\bm{\nabla}\bm{u}^\top\right)
\end{align*}
The symmetric stress tensor, 
\begin{equation*}
\bm{\nabla}\bm{u}^s=\frac{1}{2}\left(\bm{\nabla}\bm{u}+\bm{\nabla}\bm{u}^\top\right)
\end{equation*}

The weak form of the Stokes equations is given as,
\begin{align*}
-\int_{\Omega}{\bm{w}\bm{\nabla}\cdot \bm{\sigma} d\Omega}&=\int_{\Omega}{\bm{b}\cdot\bm{w}d\Omega}\\
\int_{\Omega}{q\bm{\nabla}\cdot \bm{u}d\Omega}&=0
\end{align*}
Applying the integration by parts,
\begin{align*}
\int_{\Omega}{\bm{w}\bm{\nabla}\cdot \bm{\sigma}\ d\Omega}&=\int_{\Gamma}{\bm{w}\bm{\sigma}\cdot\bm{n}\ d\Gamma}-\int_{\Omega}{\nabla w:\bm{\sigma}\ d\Omega}\\
&=\int_{\Gamma}{\bm{w}\bm{\sigma}\cdot\bm{n}\ d\Gamma}-\int_{\Omega}{\bm{\nabla w: \nabla u^s}}+\int_{\Omega}{p\bm{\nabla\cdot w}\ d\Omega}
\end{align*}

Substituting the above, we can write the whole discretization in short form as follows,
\begin{equation*}
a(\bm{w},\bm{u})+b(\bm{w},p)+b(\bm{u},q)=(\bm{w},\bm{b})+(\bm{w},\bm{t})_{\Gamma_N}
\end{equation*}
where, each term is written as,
\begin{align*}
a(\bm{w},\bm{u})&=\int_{\Omega}{\bm{\nabla w:2\mu \nabla u^s}\ d\Omega}\\
b(\bm{u},q)&=-\int_{\Omega}{q\bm{\nabla\cdot u}\ d\Omega}\\
(\bm{w},\bm{b})&=\int_{\Omega}{\bm{w}\cdot\bm{b}\ d\Omega}\\
(\bm{w},\bm{t})_{\Gamma_N}&=\int_{\Gamma_N}{\bm{w}\cdot\bm{t}\ d\Gamma}
\end{align*}

The first term can be written as,
\begin{equation*}
a(\bm{w},\bm{u})=\int_{\Omega}{\bm{\epsilon (w)}^\top \bm{\tau(u)}\ d\Omega}
\end{equation*}
where,
\begin{equation*}
\bm{\epsilon (u)}^\top=\left(\frac{\partial u_1}{\partial x_1},\frac{\partial u_2}{\partial x_2},\frac{\partial u_3}{\partial x_3},\frac{\partial u_2}{\partial x_1}+\frac{\partial u_1}{\partial x_2},\frac{\partial u_2}{\partial x_3}+\frac{\partial u_3}{\partial x_2},\frac{\partial u_3}{\partial x_1}+\frac{\partial u_1}{\partial x_3}\right)
\end{equation*}
The finite element discretization of the Stokes equation gives raise to saddle point problem, and hence the velocity and pressure spaces used in an element should be inf-sup stable.

\section{Velocity-pressure formulation}
The governing equations together with the boundary conditions are
\begin{align*}
-\nu\bm{\Delta u}+\bm{\nabla}p&=\bm{b}\ & \textrm{on}\ \Omega \  & \textrm{(Equilibrium)}\\
\bm{\nabla}\cdot \bm{u}&=0 & \textrm{on}\ \Omega  &\textrm{(Incompressibility)}\\
\bm{u}&=\bm{u_D} & \textrm{on}\ \Gamma_D &\textrm{(Dirichlet b.c.)}\\
-p\bm{n}+\nu(\bm{n\cdot\nabla}) \bm{v}&=\bm{t} & \textrm{on}\ \Gamma_N &\textrm{(Neumann b.c.)}\\
\end{align*}
It is to be noted that the Neumann boundary condition mentioned above does not involve physical traction as used in the stress formulation. However, it is a pseudo-traction. The physical traction based on Cauchy stress tensor is,
\begin{equation}
\bm{\sigma\cdot n}=-p\bm{n}+\nu\bm{n\cdot\nabla}^s \bm{v}\ne -p\bm{n}+\nu\bm{n\cdot\nabla} \bm{v}
\end{equation}

The finite element weak form is given as,
\begin{equation*}
a(\bm{w},\bm{u})+b(\bm{w},p)+b(\bm{u},q)=(\bm{w},\bm{b})+(\bm{w},\bm{t})_{\Gamma_N}
\end{equation*}
where, each term is written as,
\begin{align*}
a(\bm{w},\bm{u})&=\int_{\Omega}{\bm{\nabla w:\nu \nabla u}\ d\Omega}\\
b(\bm{u},q)&=-\int_{\Omega}{q\bm{\nabla\cdot u}\ d\Omega}\\
(\bm{w},\bm{b})&=\int_{\Omega}{\bm{w}\cdot\bm{b}\ d\Omega}\\
(\bm{w},\bm{t})_{\Gamma_N}&=\int_{\Gamma_N}{\bm{w}\cdot\bm{t}\ d\Gamma}
\end{align*}
Observe that except the first term, all the remaining contributions are the same as the stress formulation. However, $\bm{t}$ is different in both formulations.



\subsection*{References}
\begin{enumerate}
\item J. Donea and A. Huerta, \textit{Finite element methods for flow problems}, Wiley (2003).
\end{enumerate}
\end{document} 
