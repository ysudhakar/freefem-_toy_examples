//====================================================
//  Author: Y Sudhakar
//  email : iitm.sudhakar@gmail.com
//====================================================

Solve Stokes equation using Freefem++.

See description.pdf for the Problem definition and the associated weakforms

The folders contain the following codes:
1. stress_formulation --- We start with strong form of the governing equations written in terms of stresses and derive weak form
2. velo-pressure_formulation --- Newton law of viscosity is used to obtain weak form in terms of velocity and pressure, and then weak form is derived
